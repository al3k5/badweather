package com.learning.badweather

import com.learning.badweather.model.view.MainInfoModel
import com.learning.badweather.model.view.Phenomenon
import com.learning.badweather.model.view.Place
import com.learning.badweather.model.view.WeatherData
import com.learning.badweather.model.view.WindData
import com.learning.badweather.model.view.WindDirection
import org.threeten.bp.LocalDate

val MAIN_INFO_MODEL = MainInfoModel(
    LocalDate.now(),
    5,
    2,
    "Snow storm",
    Phenomenon.SNOWSTORM,
    "Some text",
    "Some sea text",
    "Some Peipsi text",
    listOf(
        WindData(WindDirection.SOUTH, 30, "Väike-Maarja")
    ),
    listOf(
        WeatherData(true, Phenomenon.SNOWSTORM, 2, "01/01")
    ),
    listOf(
        WeatherData(true, Phenomenon.SNOWSTORM, 2, "Väike-Maarja")
    )
)

val PLACE = Place(
    LocalDate.now(),
    "Väike-Maarja",
    Phenomenon.DRIFTING_SNOW,
    "Drifting snow",
    2,
    4
)
