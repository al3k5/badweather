package com.learning.badweather

import com.learning.badweather.daynight.Purpose
import com.learning.badweather.main.ChangeTab
import com.learning.badweather.main.GetInitialTabValue
import com.learning.badweather.main.Initial
import com.learning.badweather.main.MainEffect
import com.learning.badweather.main.MainEvent
import com.learning.badweather.main.MainInfoModelLoaded
import com.learning.badweather.main.MainModel
import com.learning.badweather.main.NavigateToPlace
import com.learning.badweather.main.ObserveData
import com.learning.badweather.main.PlaceClicked
import com.learning.badweather.main.PlaceLoaded
import com.learning.badweather.main.RefreshData
import com.learning.badweather.main.TabChanged
import com.learning.badweather.main.TabLoaded
import com.learning.badweather.main.mainUpdate
import com.spotify.mobius.test.NextMatchers.hasEffects
import com.spotify.mobius.test.NextMatchers.hasModel
import com.spotify.mobius.test.NextMatchers.hasNoEffects
import com.spotify.mobius.test.NextMatchers.hasNoModel
import com.spotify.mobius.test.UpdateSpec
import org.junit.Before
import org.junit.Test

class MainUpdateTest {

    private lateinit var updateSpec:
            UpdateSpec<MainModel, MainEvent, MainEffect>

    @Before
    fun before() {
        updateSpec = UpdateSpec(::mainUpdate)
    }

    @Test
    fun initial_purpose_refreshDataGetInitialTabValue() {
        val model = MainModel()
        val purpose = Purpose.AllWeather
        updateSpec
            .given(model)
            .whenEvent(Initial(purpose))
            .then(
                UpdateSpec.assertThatNext(
                    hasModel(model.copy(purpose = purpose)),
                    hasEffects(
                        RefreshData, GetInitialTabValue
                    )
                )
            )
    }

    @Test
    fun mainInfoModelLoaded_mainInfoModel() {
        val model = MainModel()
        updateSpec
            .given(model)
            .whenEvent(MainInfoModelLoaded(MAIN_INFO_MODEL))
            .then(
                UpdateSpec.assertThatNext(
                    hasModel(model.copy(mainInfoModel = MAIN_INFO_MODEL, hasData = true)),
                    hasNoEffects()
                )
            )
    }

    @Test
    fun placeLoaded_place() {
        val model = MainModel()
        updateSpec
            .given(model)
            .whenEvent(PlaceLoaded(PLACE))
            .then(
                UpdateSpec.assertThatNext(
                    hasModel(model.copy(place = PLACE)),
                    hasNoEffects()
                )
            )
    }

    @Test
    fun tabChanged_isDay_changeTab() {
        val purpose = Purpose.AllWeather
        val model = MainModel(purpose = purpose)
        updateSpec
            .given(model)
            .whenEvent(TabChanged(false))
            .then(
                UpdateSpec.assertThatNext<MainModel, MainEffect>(
                    hasModel(model.copy(isDay = false)),
                    hasEffects(
                        ChangeTab(isDay = false),
                        ObserveData(isDay = false, purpose = purpose)
                    )
                )
            )
    }

    @Test
    fun placeClicked_navigateToPlace() {
        val model = MainModel()
        updateSpec
            .given(model)
            .whenEvent(PlaceClicked(3))
            .then(
                UpdateSpec.assertThatNext<MainModel, MainEffect>(
                    hasNoModel(),
                    hasEffects(NavigateToPlace(3))
                )
            )
    }

    @Test
    fun tabLoaded_isDay_observeData() {
        val model = MainModel(purpose = Purpose.AllWeather)
        updateSpec
            .given(model)
            .whenEvent(TabLoaded(false))
            .then(
                UpdateSpec.assertThatNext<MainModel, MainEffect>(
                    hasModel(model.copy(isDay = false)),
                    hasEffects(ObserveData(false, Purpose.AllWeather))
                )
            )
    }
}
