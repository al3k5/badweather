package com.learning.badweather

import com.learning.badweather.model.dto.mapWindDirection
import com.learning.badweather.model.utils.formatDate
import com.learning.badweather.model.view.Forecast
import com.learning.badweather.model.view.MainInfoModel
import com.learning.badweather.model.view.Phenomenon
import com.learning.badweather.model.view.Place
import com.learning.badweather.model.view.Weather
import com.learning.badweather.model.view.Wind
import com.learning.badweather.model.view.mapPhenomenon
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Month

class MainInfoModelTest {

    @Test
    fun test() {
        val date = LocalDate.now()

        // Day
        val dayPhenomenonText = "Clear"
        val dayPhenomenon = mapPhenomenon(dayPhenomenonText)
        val dayTempMin = -20
        val dayTempMax = 20
        val dayText = "Some text"
        val daySea = "Some sea text"
        val dayPeipsi = "Some peipsi text"

        val dayPlaceDate1 = LocalDate.now()
        val dayPlaceName1 = "PlaceName1"
        val dayPlacePhenomenonText1 = "Light rain"
        val dayPlacePhenomenon1 = mapPhenomenon(dayPlacePhenomenonText1)
        val dayPlaceTempMin1 = null
        val dayPlacetempMax1 = 0
        val dayPlace1 = Place(
            dayPlaceDate1,
            dayPlaceName1,
            dayPlacePhenomenon1,
            dayPlacePhenomenonText1,
            dayPlaceTempMin1,
            dayPlacetempMax1
        )
        val dayPlaceDate2 = LocalDate.now()
        val dayPlaceName2 = "PlaceName2"
        val dayPlacePhenomenonText2 = "Thunderstorm"
        val dayPlacePhenomenon2 = mapPhenomenon(dayPlacePhenomenonText2)
        val dayPlaceTempMin2 = 10
        val dayPlacetempMax2 = null
        val dayPlace2 = Place(
            dayPlaceDate2,
            dayPlaceName2,
            dayPlacePhenomenon2,
            dayPlacePhenomenonText2,
            dayPlaceTempMin2,
            dayPlacetempMax2
        )
        val dayWindName1 = "WindName1"
        val dayWindDirection1 = mapWindDirection("South wind")
        val daySpeedMin1 = 12
        val daySpeedMax1 = 0
        val dayWind1 = Wind(dayWindName1, dayWindDirection1, daySpeedMin1, daySpeedMax1)

        val dayWindName2 = "WindName2"
        val dayWindDirection2 = mapWindDirection("East wind")
        val daySpeedMin2 = 13
        val daySpeedMax2 = -5
        val dayWind2 = Wind(dayWindName2, dayWindDirection2, daySpeedMin2, daySpeedMax2)

        // Night
        val nightPhenomenonText = "Risk of glaze"
        val nightPhenomenon = mapPhenomenon(dayPhenomenonText)
        val nightTempMin = -10
        val nightTempMax = 10
        val nightText = "Some text night"
        val nightSea = "Some sea text night"
        val nightPeipsi = "Some peipsi text night"

        val nightPlaceDate1 = LocalDate.now()
        val nightPlaceName1 = "PlaceName1 night"
        val nightPlacePhenomenonText1 = "Cloudy with clear spells"
        val nightPlacePhenomenon1 = mapPhenomenon(nightPlacePhenomenonText1)
        val nightPlaceTempMin1 = 12
        val nightPlacetempMax1 = -3
        val nightPlace1 = Place(
            nightPlaceDate1,
            nightPlaceName1,
            nightPlacePhenomenon1,
            nightPlacePhenomenonText1,
            nightPlaceTempMin1,
            nightPlacetempMax1
        )
        val nightPlaceDate2 = LocalDate.of(1991, Month.APRIL, 12)
        val nightPlaceName2 = "PlaceName2 night"
        val nightPlacePhenomenonText2 = "Few clouds"
        val nightPlacePhenomenon2 = mapPhenomenon(nightPlacePhenomenonText2)
        val nightPlaceTempMin2 = null
        val nightPlacetempMax2 = 1000
        val nightPlace2 = Place(
            nightPlaceDate2,
            nightPlaceName2,
            nightPlacePhenomenon2,
            nightPlacePhenomenonText2,
            nightPlaceTempMin2,
            nightPlacetempMax2
        )
        val nightWindName1 = "WindName1 night"
        val nightWindDirection1 = mapWindDirection("North wind")
        val nightSpeedMin1 = 700
        val nightSpeedMax1 = -2
        val nightWind1 = Wind(nightWindName1, nightWindDirection1, nightSpeedMin1, nightSpeedMax1)

        val nightWindName2 = "WindName2 night"
        val nightWindDirection2 = mapWindDirection("West wind")
        val nightSpeedMin2 = 0
        val nightSpeedMax2 = 0
        val nightWind2 = Wind(nightWindName2, nightWindDirection2, nightSpeedMin2, nightSpeedMax2)

        val day1Forecast = Forecast(
            date,
            Weather(
                dayPhenomenonText,
                dayPhenomenon,
                dayTempMin,
                dayTempMax,
                dayText,
                daySea,
                dayPeipsi,
                listOf(
                    dayPlace1,
                    dayPlace2
                ),
                listOf(
                    dayWind1,
                    dayWind2
                )
            ),
            Weather(
                nightPhenomenonText,
                nightPhenomenon,
                nightTempMin,
                nightTempMax,
                nightText,
                nightSea,
                nightPeipsi,
                listOf(
                    nightPlace1,
                    nightPlace2
                ),
                listOf(
                    nightWind1,
                    nightWind2
                )
            )
        )

        val nextDate1 = LocalDate.of(2002, Month.AUGUST, 1)
        // Day
        val nextDay1PhenomenonText = "Hail"
        val nextDay1Phenomenon = mapPhenomenon(nextDay1PhenomenonText)
        val nextDay1TempMin = 0
        val nextDay1TempMax = -100

        // Night
        val nextNight1PhenomenonText = "Mist"
        val nextNight1Phenomenon = mapPhenomenon(dayPhenomenonText)
        val nextNight1TempMin = 3
        val nextNight1TempMax = 2
        val day2Forecast = Forecast(
            nextDate1,
            Weather(
                nextDay1PhenomenonText,
                nextDay1Phenomenon,
                nextDay1TempMin,
                nextDay1TempMax,
                "",
                "",
                "",
                listOf(),
                listOf()
            ),
            Weather(
                nextNight1PhenomenonText,
                nextNight1Phenomenon,
                nextNight1TempMin,
                nextNight1TempMax,
                "",
                "",
                "",
                listOf(),
                listOf()
            )
        )

        val nextDate2 = LocalDate.of(2002, Month.AUGUST, 1)
        // Day
        val nextDay2PhenomenonText = "Variable clouds"
        val nextDay2Phenomenon = mapPhenomenon(nextDay2PhenomenonText)
        val nextDay2TempMin = 0
        val nextDay2TempMax = -100

        // Night
        val nextNight2PhenomenonText = "Drifting snow"
        val nextNight2Phenomenon = mapPhenomenon(dayPhenomenonText)
        val nextNight2TempMin = 3
        val nextNight2TempMax = 2
        val day3Forecast = Forecast(
            nextDate2,
            Weather(
                nextDay2PhenomenonText,
                nextDay2Phenomenon,
                nextDay2TempMin,
                nextDay2TempMax,
                "",
                "",
                "",
                listOf(),
                listOf()
            ),
            Weather(
                nextNight2PhenomenonText,
                nextNight2Phenomenon,
                nextNight2TempMin,
                nextNight2TempMax,
                "",
                "",
                "",
                listOf(),
                listOf()
            )
        )

        val forecasts = listOf(
            day1Forecast,
            day2Forecast,
            day3Forecast
        )

        // Day
        val mainInfoModel1 = MainInfoModel(true, forecasts)
        val dayFirstWind = mainInfoModel1.winds.first()
        val daySecondWind = mainInfoModel1.winds[1]
        val dayFirstPlace = mainInfoModel1.placesData.first()
        val daySecondPlace = mainInfoModel1.placesData[1]
        val dayNextDayData1 = mainInfoModel1.nextDays.first()
        val dayNextDayData2 = mainInfoModel1.nextDays[1]
        assertEquals(mainInfoModel1.date, day1Forecast.date)
        assertEquals(mainInfoModel1.tempMax, dayTempMax)
        assertEquals(mainInfoModel1.tempMin, dayTempMin)
        assertEquals(mainInfoModel1.phenomenonText, dayPhenomenonText)
        assertEquals(mainInfoModel1.phenomenon, Phenomenon.CLEAR)
        assertEquals(mainInfoModel1.sea, daySea)
        assertEquals(mainInfoModel1.peipsi, dayPeipsi)
        assertEquals(mainInfoModel1.text, dayText)
        assertEquals(dayFirstWind.title, dayWindName1)
        assertEquals(dayFirstWind.windDirection, dayWindDirection1)
        assertEquals(dayFirstWind.windSpeed, daySpeedMax1)
        assertEquals(daySecondWind.title, dayWindName2)
        assertEquals(daySecondWind.windDirection, dayWindDirection2)
        assertEquals(daySecondWind.windSpeed, daySpeedMax2)
        assertEquals(dayFirstPlace.isDay, true)
        assertEquals(dayFirstPlace.phenomenon, Phenomenon.LIGHT_RAIN)
        assertEquals(dayFirstPlace.temperature, dayPlacetempMax1)
        assertEquals(dayFirstPlace.title, dayPlaceName1)
        assertEquals(daySecondPlace.isDay, true)
        assertEquals(daySecondPlace.phenomenon, Phenomenon.THUNDERSTORM)
        assertEquals(daySecondPlace.temperature, dayPlaceTempMin2)
        assertEquals(daySecondPlace.title, dayPlaceName2)
        assertEquals(dayNextDayData1.title, formatDate(day2Forecast.date).toString())
        assertEquals(dayNextDayData1.temperature, nextDay1TempMax)
        assertEquals(dayNextDayData1.phenomenon, Phenomenon.HAIL)
        assertEquals(dayNextDayData1.isDay, true)
        assertEquals(dayNextDayData2.title, formatDate(day3Forecast.date).toString())
        assertEquals(dayNextDayData2.temperature, nextDay2TempMax)
        assertEquals(dayNextDayData2.phenomenon, Phenomenon.VARIABLE_CLOUDS)
        assertEquals(dayNextDayData2.isDay, true)

        // Night
        val mainInfoModel2 = MainInfoModel(false, forecasts)
        val nightFirstWind = mainInfoModel2.winds.first()
        val nightSecondWind = mainInfoModel2.winds[1]
        val nightFirstPlace = mainInfoModel2.placesData.first()
        val nightSecondPlace = mainInfoModel2.placesData[1]
        val nightNextDayData1 = mainInfoModel2.nextDays.first()
        val nightNextDayData2 = mainInfoModel2.nextDays[1]
        assertEquals(mainInfoModel2.date, day1Forecast.date)
        assertEquals(mainInfoModel2.tempMax, nightTempMax)
        assertEquals(mainInfoModel2.tempMin, nightTempMin)
        assertEquals(mainInfoModel2.phenomenonText, nightPhenomenonText)
        assertEquals(mainInfoModel2.phenomenon, Phenomenon.CLEAR)
        assertEquals(mainInfoModel2.sea, nightSea)
        assertEquals(mainInfoModel2.peipsi, nightPeipsi)
        assertEquals(mainInfoModel2.text, nightText)
        assertEquals(nightFirstWind.title, nightWindName1)
        assertEquals(nightFirstWind.windDirection, nightWindDirection1)
        assertEquals(nightFirstWind.windSpeed, nightSpeedMax1)
        assertEquals(nightSecondWind.title, nightWindName2)
        assertEquals(nightSecondWind.windDirection, nightWindDirection2)
        assertEquals(nightSecondWind.windSpeed, nightSpeedMax2)
        assertEquals(nightFirstPlace.isDay, false)
        assertEquals(nightFirstPlace.phenomenon, Phenomenon.CLOUDY_WITH_CLEAR_SPELLS)
        assertEquals(nightFirstPlace.temperature, nightPlacetempMax1)
        assertEquals(nightFirstPlace.title, nightPlaceName1)
        assertEquals(nightSecondPlace.isDay, false)
        assertEquals(nightSecondPlace.phenomenon, Phenomenon.FEW_CLOUDS)
        assertEquals(nightSecondPlace.temperature, nightPlacetempMax2)
        assertEquals(nightSecondPlace.title, nightPlaceName2)
        assertEquals(nightNextDayData1.title, formatDate(day2Forecast.date).toString())
        assertEquals(nightNextDayData1.temperature, nextNight1TempMax)
        assertEquals(nightNextDayData1.phenomenon, Phenomenon.CLEAR)
        assertEquals(nightNextDayData1.isDay, false)
        assertEquals(nightNextDayData2.title, formatDate(day3Forecast.date).toString())
        assertEquals(nightNextDayData2.temperature, nextNight2TempMax)
        assertEquals(nightNextDayData2.phenomenon, Phenomenon.CLEAR)
        assertEquals(nightNextDayData2.isDay, false)
    }
}
