package com.learning.badweather.main

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.jakewharton.rxbinding3.viewpager2.pageSelections
import com.learning.badweather.R
import com.learning.badweather.daynight.Purpose
import com.learning.badweather.getViewModel
import com.learning.badweather.model.utils.formatDate
import com.learning.badweather.widget.PageFadeTransformer
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment(R.layout.main_fragment) {

    private var disposable: Disposable? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagerAdapter = ScreenSlidePagerAdapter(this, null)
        viewPager.adapter = pagerAdapter
        viewPager.isNestedScrollingEnabled = true
        //viewPager.setPageTransformer(PageFadeTransformer())
        TabLayoutMediator(tabLayout, viewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                if (position == 0) {
                    tab.text = getString(R.string.main_day)
                } else {
                    tab.text = getString(R.string.main_night)
                }
            }).attach()
    }

    override fun onStart() {
        super.onStart()

        disposable =
            Observable.mergeArray(Observable.just(Unit).map { Initial(Purpose.AllWeather) },
                viewPager.pageSelections().map { TabChanged(it == 0) }
            )
                .compose(getViewModel(MainViewModel::class))
                .subscribe { model ->
                    if (model.mainInfoModel != null) {
                        toolbarTitle.text =
                            getString(R.string.main_title, formatDate(model.mainInfoModel.date))
                    }
                    if (model.isDay != null) {
                        viewPager.currentItem = if (model.isDay) 0 else 1
                        backgroundImage.setImageResource(
                            if (model.isDay) {
                                R.drawable.day_gradient
                            } else {
                                R.drawable.night_gradient
                            }
                        )
                        requireActivity().window.statusBarColor = if (model.isDay) {
                            ContextCompat.getColor(requireContext(), R.color.colorBlue)
                        } else {
                            ContextCompat.getColor(requireContext(), R.color.colorGrey)
                        }
                    }
                }
    }

    override fun onStop() {
        disposable?.dispose()
        super.onStop()
    }
}
