package com.learning.badweather.main

import com.learning.badweather.daynight.Purpose
import com.learning.badweather.model.view.MainInfoModel
import com.learning.badweather.model.view.Place

sealed class MainEvent

data class Initial(val purpose: Purpose) : MainEvent()

data class MainInfoModelLoaded(val mainInfoModel: MainInfoModel?) : MainEvent()

data class PlaceLoaded(val place: Place) : MainEvent()

data class TabChanged(val isDay: Boolean) : MainEvent()

data class TabLoaded(val isDay: Boolean) : MainEvent()

data class PlaceClicked(val index: Int) : MainEvent()
