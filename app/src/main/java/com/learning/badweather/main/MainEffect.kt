package com.learning.badweather.main

import com.learning.badweather.daynight.Purpose

sealed class MainEffect

object RefreshData : MainEffect()

data class ObserveData(val isDay: Boolean, val purpose: Purpose) : MainEffect()

data class NavigateToPlace(val index: Int) : MainEffect()

object GetInitialTabValue : MainEffect()

data class ChangeTab(val isDay: Boolean) : MainEffect()
