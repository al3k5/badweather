package com.learning.badweather.main

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.learning.badweather.daynight.DayNightFragment
import com.learning.badweather.daynight.Purpose

class ScreenSlidePagerAdapter(fragment: Fragment, private val index: Int?) :
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return if (index == null) {
            DayNightFragment.getInstance(Purpose.AllWeather)
        } else {
            DayNightFragment.getInstance(Purpose.PlaceWeather(index))
        }
    }
}
