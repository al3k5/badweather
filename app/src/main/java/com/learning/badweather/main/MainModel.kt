package com.learning.badweather.main

import com.learning.badweather.daynight.Purpose
import com.learning.badweather.model.view.MainInfoModel
import com.learning.badweather.model.view.Place

data class MainModel(
    val isDay: Boolean? = null,
    val hasData: Boolean = false,
    val place: Place? = null,
    val mainInfoModel: MainInfoModel? = null,
    val purpose: Purpose? = null
)
