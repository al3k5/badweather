package com.learning.badweather.main

import android.content.SharedPreferences
import android.util.Log
import com.learning.badweather.BaseViewModel
import com.learning.badweather.daynight.Purpose
import com.learning.badweather.model.repository.WeatherRepository
import com.learning.badweather.model.view.MainInfoModel
import com.learning.badweather.utils.Navigator
import com.spotify.mobius.Next
import com.spotify.mobius.Next.dispatch
import com.spotify.mobius.Next.next
import com.spotify.mobius.Update
import com.spotify.mobius.rx2.RxMobius
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

fun mainUpdate(
    model: MainModel,
    event: MainEvent
): Next<MainModel, MainEffect> {
    return when (event) {
        is Initial -> next(
            model.copy(purpose = event.purpose),
            setOf(RefreshData, GetInitialTabValue)
        )
        is MainInfoModelLoaded -> next(model.copy(mainInfoModel = event.mainInfoModel, hasData = true))
        is PlaceLoaded -> next(model.copy(place = event.place))
        is TabChanged -> next(
            model.copy(isDay = event.isDay, mainInfoModel = null, hasData = false),
            setOf(ObserveData(event.isDay, model.purpose!!), ChangeTab(event.isDay))
        )
        is PlaceClicked -> dispatch<MainModel, MainEffect>(setOf(NavigateToPlace(event.index)))
        is TabLoaded -> next(
            model.copy(isDay = event.isDay),
            setOf(ObserveData(event.isDay, model.purpose!!))
        )
    }
}

class MainViewModel @Inject constructor(
    weatherRepository: WeatherRepository,
    navigator: Navigator,
    sharedPreferences: SharedPreferences
) :
    BaseViewModel<MainModel, MainEvent, MainEffect>(
        "MainViewModel",
        Update(::mainUpdate),
        MainModel(),
        RxMobius.subtypeEffectHandler<MainEffect, MainEvent>()
            .addTransformer(ObserveData::class.java) { upstream ->
                upstream.switchMap { effect ->
                    weatherRepository.get()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map {
                            if (effect.purpose is Purpose.PlaceWeather) {
                                PlaceLoaded(
                                    if (effect.isDay) {
                                        it.first().day.places[effect.purpose.index]
                                    } else {
                                        it.first().night.places[effect.purpose.index]
                                    }
                                )
                            } else {
                                MainInfoModelLoaded(
                                    if (it.isEmpty()) {
                                        null
                                    } else {
                                        MainInfoModel(
                                            effect.isDay,
                                            it
                                        )
                                    }
                                )
                            }
                        }
                }
            }
            .addTransformer(RefreshData::class.java) { upstream ->
                upstream.switchMap {
                    weatherRepository.refresh()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(::logError)
                        .onErrorComplete()
                        .toObservable<MainEvent>()
                }
            }
            .addTransformer(GetInitialTabValue::class.java) { upstream ->
                upstream.switchMap {
                    Single.fromCallable {
                        TabLoaded(
                            sharedPreferences.getBoolean(
                                PREF_TAB,
                                true
                            )
                        )
                    }
                        .toObservable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                }
            }
            .addTransformer(ChangeTab::class.java) { upstream ->
                upstream.switchMap { effect ->
                    Completable.fromAction {
                        sharedPreferences.edit()
                            .putBoolean(PREF_TAB, effect.isDay).apply()
                    }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .toObservable<MainEvent>()
                }
            }
            .addConsumer(NavigateToPlace::class.java) {
                navigator.to(MainFragmentDirections.place(it.index))
            }
            .build()
    )

private fun logError(e: Throwable) {
    Log.e("Error", e.message, e)
}

const val PREF_TAB = "tab"
