package com.learning.badweather.widget

import android.view.View
import androidx.viewpager2.widget.ViewPager2

class PageFadeTransformer : ViewPager2.PageTransformer {
    override fun transformPage(page: View, position: Float) {
        page.alpha = 0f
        page.visibility = View.VISIBLE

        page.animate()
            .alpha(1f).duration =
            page.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
    }
}
