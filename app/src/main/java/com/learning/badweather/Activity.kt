package com.learning.badweather

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.learning.badweather.utils.ActivityService

class Activity : AppCompatActivity() {

    private lateinit var activityService: ActivityService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        activityService = applicationContext.applicationComponent.activityService()
        activityService.onCreate(this)
    }

    override fun onDestroy() {
        activityService.onDestroy(this)
        super.onDestroy()
    }
}
