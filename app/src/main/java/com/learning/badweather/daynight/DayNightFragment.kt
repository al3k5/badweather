package com.learning.badweather.daynight

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.fragment.app.Fragment
import com.learning.badweather.R
import com.learning.badweather.daynight.widget.DayNightAdapter
import com.learning.badweather.getViewModel
import com.learning.badweather.main.MainViewModel
import com.learning.badweather.main.PlaceClicked
import io.reactivex.disposables.Disposable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.day_night_fragment.*

class DayNightFragment : Fragment(R.layout.day_night_fragment) {

    companion object {
        fun getInstance(purpose: Purpose): Fragment {
            val fragment = DayNightFragment()
            val arg = Bundle()
            arg.putParcelable(PURPOSE, purpose)
            fragment.arguments = arg
            return fragment
        }
    }

    private var purpose: Purpose? = null
    private lateinit var adapter: DayNightAdapter
    private var disposable: Disposable? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        purpose = arguments?.getParcelable(PURPOSE)
        adapter = DayNightAdapter()
        viewPager.adapter = adapter
    }

    override fun onStart() {
        super.onStart()

        disposable = adapter.placeClicks.map { PlaceClicked(it) }
            .compose(parentFragment?.getViewModel(MainViewModel::class))
            .subscribe { model ->
                if (model.isDay != null) {
                    if (purpose is Purpose.PlaceWeather) {
                        if (model.place != null) {
                            adapter.submitPlaceData(model.isDay, model.place)
                        }
                    } else {
                        if (model.hasData) {
                            adapter.submitMainData(model.isDay, model.mainInfoModel)
                        }
                    }
                }
            }
    }

    override fun onStop() {
        disposable?.dispose()
        super.onStop()
    }
}

const val PURPOSE = "purpose"

sealed class Purpose : Parcelable {

    @Parcelize
    object AllWeather : Purpose()

    @Parcelize
    data class PlaceWeather(val index: Int) : Purpose()
}
