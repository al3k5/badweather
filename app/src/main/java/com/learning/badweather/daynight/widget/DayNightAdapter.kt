package com.learning.badweather.daynight.widget

import android.annotation.SuppressLint
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.jakewharton.rxbinding3.view.clicks
import com.learning.badweather.R
import com.learning.badweather.model.view.MainInfoModel
import com.learning.badweather.model.view.PhenomenonAnimation
import com.learning.badweather.model.view.Place
import com.learning.badweather.model.view.WeatherData
import com.learning.badweather.model.view.WindData
import com.learning.badweather.model.view.getWindRotation
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.tab_bottom.*
import kotlinx.android.synthetic.main.tab_empty.*
import kotlinx.android.synthetic.main.tab_top.*

class DayNightAdapter : ListAdapter<Item, ItemViewHolder<Item>>(DiffUtilCallback()) {

    private val placeClicksSubject = PublishSubject.create<Int>()
    val placeClicks: Observable<Int> = placeClicksSubject

    fun submitPlaceData(isDay: Boolean, place: Place) {
        submitList(listOf(PlaceItem(isDay, place)))
    }

    fun submitMainData(isDay: Boolean, mainInfoModel: MainInfoModel?) {
        if (mainInfoModel == null) {
            submitList(listOf(EmptyItem(isDay)))
        } else {
            submitList(listOf(TopItem(isDay, mainInfoModel), BottomItem(isDay, mainInfoModel)))
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Item> {
        val view = LayoutInflater.from(parent.context)
            .inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.tab_top -> TopViewHolder(view) as ItemViewHolder<Item>
            R.layout.tab_bottom -> BottomViewHolder(
                view,
                placeClicksSubject
            ) as ItemViewHolder<Item>
            R.layout.tab_empty -> EmptyViewHolder(view) as ItemViewHolder<Item>
            R.layout.tab_place -> PlaceViewHolder(view) as ItemViewHolder<Item>
            else -> throw IllegalArgumentException("Unknown viewType: $viewType")
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder<Item>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is TopItem -> R.layout.tab_top
            is BottomItem -> R.layout.tab_bottom
            is EmptyItem -> R.layout.tab_empty
            is PlaceItem -> R.layout.tab_place
        }
    }
}

sealed class Item

data class TopItem(val isDay: Boolean, val mainInfoModel: MainInfoModel) : Item()

data class BottomItem(val isDay: Boolean, val mainInfoModel: MainInfoModel) : Item()

data class EmptyItem(val isDay: Boolean) : Item()

data class PlaceItem(val isDay: Boolean, val place: Place) : Item()

private class DiffUtilCallback : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem == newItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
        return oldItem == newItem
    }
}

sealed class ItemViewHolder<T : Item>(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    open fun bind(item: T) {}
}

class TopViewHolder(override val containerView: View) :
    ItemViewHolder<TopItem>(containerView) {

    private var valuesIndex = 0
    private val context = containerView.context
    private val handler = Handler()

    override fun bind(item: TopItem) {
        val mainInfoModel = item.mainInfoModel
        tempValueView.text = context.getString(R.string.temp_value, mainInfoModel.tempMax)
        tempMinMax.text =
            context.getString(
                R.string.temp_min_max_value,
                mainInfoModel.tempMin,
                mainInfoModel.tempMax
            )
        phenomenonAnimationView.setAnimation(
            PhenomenonAnimation(
                item.isDay,
                mainInfoModel.phenomenon
            )
        )
        phenomenonLabel.text = mainInfoModel.phenomenonText
        val titles = mutableListOf<String>()
        val texts = mutableListOf<String>()
        if (mainInfoModel.text.isNotEmpty()) {
            titles.add(context.getString(R.string.day_night_weather_info))
            texts.add(mainInfoModel.text)
        }
        if (mainInfoModel.sea.isNotEmpty()) {
            titles.add(context.getString(R.string.day_night_sea))
            texts.add(mainInfoModel.sea)
        }
        if (mainInfoModel.peipsi.isNotEmpty()) {
            titles.add(context.getString(R.string.day_night_peipsi))
            texts.add(mainInfoModel.peipsi)
        }

        val animationIn = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
        animationIn.duration = 500
        val animationOut = AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
        animationOut.duration = 500
        titleSwitcher.inAnimation = animationIn
        titleSwitcher.outAnimation = animationOut
        textSwitcher.inAnimation = animationIn
        textSwitcher.outAnimation = animationOut

        handler.removeCallbacksAndMessages(null)
        handler.post(object : Runnable {
            override fun run() {
                if (valuesIndex >= texts.size) valuesIndex = 0
                titleSwitcher.setText(titles[valuesIndex])
                textSwitcher.setText(texts[valuesIndex])
                valuesIndex++
                handler.postDelayed(this, 10000)
            }
        })
    }
}

class BottomViewHolder(
    override val containerView: View,
    private val placeClicksSubject: Subject<Int>
) :
    ItemViewHolder<BottomItem>(containerView) {

    override fun bind(item: BottomItem) {
        val mainInfoModel = item.mainInfoModel

        nextDaysRecyclerView.layoutManager =
            LinearLayoutManager(containerView.context, LinearLayoutManager.HORIZONTAL, false)
        val nextDaysAdapter = WeatherAdapter(mainInfoModel.nextDays, null)
        nextDaysRecyclerView.adapter = nextDaysAdapter

        windRecyclerView.layoutManager =
            LinearLayoutManager(containerView.context, LinearLayoutManager.HORIZONTAL, false)
        val windAdapter = WindAdapter(mainInfoModel.winds)
        windRecyclerView.adapter = windAdapter

        placesRecyclerView.layoutManager =
            LinearLayoutManager(containerView.context, LinearLayoutManager.HORIZONTAL, false)
        val placesAdapter = WeatherAdapter(mainInfoModel.placesData, placeClicksSubject)
        placesRecyclerView.adapter = placesAdapter
    }

    private class WindAdapter(private val data: List<WindData>) :
        RecyclerView.Adapter<WindAdapter.MyViewHolder>() {

        class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder {
            return MyViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.horizontal_wind_item, parent, false)
            )
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val windData = data[position]
            holder.view.findViewById<LottieAnimationView>(R.id.windDirectionAnimationView)
                .rotation = getWindRotation(windData.windDirection)
            holder.view.findViewById<TextView>(R.id.windSpeedView).text =
                holder.view.context.getString(R.string.wind_speed_value, windData.windSpeed)
            holder.view.findViewById<TextView>(R.id.titleView).text = windData.title
        }

        override fun getItemCount() = data.size
    }

    private class WeatherAdapter(
        private val data: List<WeatherData>,
        private val placeClicksSubject: Subject<Int>?
    ) :
        RecyclerView.Adapter<WeatherAdapter.MyViewHolder>() {

        class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view)

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder {
            return MyViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.horizontal_weather_item, parent, false)
            )
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val weatherData = data[position]
            holder.view.findViewById<LottieAnimationView>(R.id.phenomenonAnimationView)
                .setAnimation(
                    PhenomenonAnimation(weatherData.isDay, weatherData.phenomenon)
                )
            holder.view.findViewById<TextView>(R.id.temperatureView).text =
                holder.view.context.getString(R.string.temp_value, weatherData.temperature)
            holder.view.findViewById<TextView>(R.id.titleView).text = weatherData.title
            if (placeClicksSubject != null) {
                holder.view.findViewById<View>(R.id.weatherItem).clicks().map { position }
                    .subscribe(placeClicksSubject)
            }
        }

        override fun getItemCount() = data.size
    }
}

class EmptyViewHolder(override val containerView: View) :
    ItemViewHolder<EmptyItem>(containerView) {

    override fun bind(item: EmptyItem) {
        errorAnimationView.setAnimation(
            if (item.isDay) {
                "partly_cloudy.json"
            } else {
                "cloudynight.json"
            }
        )
    }
}

class PlaceViewHolder(override val containerView: View) :
    ItemViewHolder<PlaceItem>(containerView) {

    private val context = containerView.context
    override fun bind(item: PlaceItem) {

        val place = item.place
        tempValueView.text =
            context.getString(R.string.temp_value, place.tempMax ?: place.tempmin)
        if (place.tempmin != null && item.place.tempMax != null) {
            tempMinMax.text =
                context.getString(R.string.temp_min_max_value, place.tempmin, place.tempMax)
        }
        phenomenonAnimationView.setAnimation(PhenomenonAnimation(item.isDay, place.phenomenon))
        phenomenonLabel.text = place.phenomenonText
    }
}
