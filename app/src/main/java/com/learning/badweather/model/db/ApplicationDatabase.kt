package com.learning.badweather.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.learning.badweather.model.dto.ForecastDto
import com.learning.badweather.model.dto.PlaceDto
import com.learning.badweather.model.dto.WindDto

@Database(
    entities = [ForecastDto::class, WindDto::class, PlaceDto::class],
    version = 1,
    exportSchema = false
)
abstract class ApplicationDatabase : RoomDatabase() {

    abstract fun forecastsDao(): ForecastsDao
}
