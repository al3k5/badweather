package com.learning.badweather.model.view

import org.threeten.bp.LocalDate

data class Place(
    val date: LocalDate,
    val name: String,
    val phenomenon: Phenomenon,
    val phenomenonText: String,
    val tempmin: Int?,
    val tempMax: Int?
)
