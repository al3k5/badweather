package com.learning.badweather.model.view

enum class WindDirection {
    NORTH, NORTH_EAST, EAST, SOUTH_EAST, SOUTH, SOUTH_WEST, WEST, NORTH_WEST
}

fun getWindRotation(windDirection: WindDirection): Float {
    return when (windDirection) {
        WindDirection.WEST -> 0F
        WindDirection.NORTH_WEST -> 45F
        WindDirection.NORTH -> 90F
        WindDirection.NORTH_EAST -> 135F
        WindDirection.EAST -> 180F
        WindDirection.SOUTH_EAST -> 225F
        WindDirection.SOUTH -> 270F
        WindDirection.SOUTH_WEST -> 315F
    }
}
