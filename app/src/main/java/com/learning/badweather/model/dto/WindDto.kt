package com.learning.badweather.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.learning.badweather.model.view.Wind
import com.learning.badweather.model.view.WindDirection
import com.squareup.moshi.JsonClass

@Entity
@JsonClass(generateAdapter = true)
data class WindDto(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    val name: String,
    val date: String?,
    val isDay: Boolean?,
    val direction: String,
    val speedmin: Int,
    val speedmax: Int
)

fun WindDto.toWind(): Wind {
    return Wind(
        name = this.name,
        speedMin = this.speedmin,
        speexMax = this.speedmax,
        windDirection = mapWindDirection(this.direction)
    )
}

fun mapWindDirection(direction: String): WindDirection {
    return when (direction) {
        "Southwest wind" -> WindDirection.SOUTH_WEST
        "West wind" -> WindDirection.WEST
        "Northwest wind" -> WindDirection.NORTH_WEST
        "North wind" -> WindDirection.NORTH
        "Northeast wind" -> WindDirection.NORTH_EAST
        "East wind" -> WindDirection.EAST
        "Southeast wind" -> WindDirection.SOUTH_EAST
        "South wind" -> WindDirection.SOUTH
        else -> throw IllegalArgumentException("Unknown wind direction")
    }
}
