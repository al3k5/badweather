package com.learning.badweather.model.dto

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.learning.badweather.model.view.Forecast
import com.learning.badweather.model.view.Place
import com.learning.badweather.model.view.Weather
import com.learning.badweather.model.view.mapPhenomenon
import com.squareup.moshi.JsonClass
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

@Entity
@JsonClass(generateAdapter = true)
data class ForecastDto(
    @PrimaryKey val date: String,
    @Embedded(prefix = "day_") val day: WeatherDto,
    @Embedded(prefix = ("night_")) val night: WeatherDto
)

fun ForecastDto.toForecast(): Forecast {
    return Forecast(
        date = mapDate(this.date),
        day = Weather(
            phenomenonText = this.day.phenomenon,
            phenomenon = mapPhenomenon(this.day.phenomenon),
            tempmin = this.day.tempmin,
            tempMax = this.day.tempmax,
            text = this.day.text,
            sea = this.day.sea ?: "",
            peipsi = this.day.peipsi ?: "",
            places = this.day.places?.map { mapPlace(this.date, it) } ?: listOf(),
            winds = this.day.winds?.map { it.toWind() } ?: listOf()
        ),
        night = Weather(
            phenomenonText = this.night.phenomenon,
            phenomenon = mapPhenomenon(this.night.phenomenon),
            tempmin = this.night.tempmin,
            tempMax = this.night.tempmax,
            text = this.night.text,
            sea = this.night.sea ?: "",
            peipsi = this.night.peipsi ?: "",
            places = this.night.places?.map { mapPlace(this.date, it) } ?: listOf(),
            winds = this.night.winds?.map { it.toWind() } ?: listOf()
        )
    )
}

private fun mapPlace(date: String, placeDto: PlaceDto): Place {
    return Place(
        date = mapDate(date),
        name = placeDto.name,
        phenomenon = mapPhenomenon(placeDto.phenomenon),
        phenomenonText = placeDto.phenomenon,
        tempMax = placeDto.tempmax,
        tempmin = placeDto.tempmin
    )
}

private fun mapDate(date: String): LocalDate {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    return LocalDate.parse(date, formatter)
}
