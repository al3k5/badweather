package com.learning.badweather.model.dto

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@Entity
@JsonClass(generateAdapter = true)
data class PlaceDto(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    val name: String,
    val date: String?,
    val isDay: Boolean?,
    val phenomenon: String,
    val tempmin: Int?,
    val tempmax: Int?
)
