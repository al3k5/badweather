package com.learning.badweather.model.db

import androidx.room.Embedded
import androidx.room.Relation
import com.learning.badweather.model.dto.ForecastDto
import com.learning.badweather.model.dto.PlaceDto
import com.learning.badweather.model.dto.WindDto

data class ForecastData(

    @Embedded
    val forecast: ForecastDto,

    @Relation(
        parentColumn = "date",
        entityColumn = "date"
    )
    val winds: List<WindDto>?,

    @Relation(
        parentColumn = "date",
        entityColumn = "date"
    )
    val places: List<PlaceDto>?
)
