package com.learning.badweather.model.dto

import androidx.room.Ignore
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WeatherDto(
    val phenomenon: String,
    val tempmin: Int,
    val tempmax: Int,
    val text: String,
    val sea: String?,
    val peipsi: String?,
    @Ignore val places: List<PlaceDto>?,
    @Ignore val winds: List<WindDto>?
) {
    constructor(
        phenomenon: String,
        tempmin: Int,
        tempmax: Int,
        text: String,
        sea: String?,
        peipsi: String?
    ) : this(
        phenomenon,
        tempmin,
        tempmax,
        text,
        sea,
        peipsi,
        null,
        null
    )
}
