package com.learning.badweather.model.utils

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

fun formatDate(localDate: LocalDate): CharSequence {
    return localDate.format(DATE_FORMATTER)
}

private val DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM")
