package com.learning.badweather.model.db

import androidx.room.*
import com.learning.badweather.model.dto.ForecastDto
import com.learning.badweather.model.dto.PlaceDto
import com.learning.badweather.model.dto.WindDto
import io.reactivex.Observable

@Dao
abstract class ForecastsDao {

    @Transaction
    open fun replaceForecasts(forecasts: List<ForecastDto>) {
        deleteForecasts()
        deleteWinds()
        deletePlaces()
        forecasts.forEach { forecast ->
            insertForecast(forecast)
            forecast.day.winds?.forEach { windDto ->
                insertWind(windDto.copy(date = forecast.date, isDay = true))
            }
            forecast.night.winds?.forEach { windDto ->
                insertWind(windDto.copy(date = forecast.date, isDay = false))
            }
            forecast.day.places?.forEach { placeDto ->
                insertPlace(placeDto.copy(date = forecast.date, isDay = true))
            }
            forecast.night.places?.forEach { placeDto ->
                insertPlace(placeDto.copy(date = forecast.date, isDay = false))
            }
        }
    }

    fun get(): Observable<List<ForecastDto>> {
        return getForecastsWithData().map { it.map { mapForecastData(it) } }
    }

    @Transaction
    @Query("SELECT * FROM ForecastDto")
    abstract fun getForecastsWithData(): Observable<List<ForecastData>>

    @Query("DELETE FROM ForecastDto")
    protected abstract fun deleteForecasts()

    @Query("DELETE FROM WindDto")
    protected abstract fun deleteWinds()

    @Query("DELETE FROM PlaceDto")
    protected abstract fun deletePlaces()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract fun insertForecast(forecast: ForecastDto)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract fun insertWind(windDto: WindDto)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    protected abstract fun insertPlace(placeDto: PlaceDto)

    private fun mapForecastData(forecastData: ForecastData): ForecastDto {
        val dayWinds = forecastData.winds?.filter { it.isDay!! }
        val nightWinds = forecastData.winds?.filter { !it.isDay!! }
        val dayPlaces = forecastData.places?.filter { it.isDay!! }
        val nightPlaces = forecastData.places?.filter { !it.isDay!! }
        return forecastData.forecast.copy(
            day = forecastData.forecast.day.copy(
                winds = dayWinds,
                places = dayPlaces
            ),
            night = forecastData.forecast.night.copy(
                winds = nightWinds,
                places = nightPlaces
            )
        )
    }
}
