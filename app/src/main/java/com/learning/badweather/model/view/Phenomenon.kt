package com.learning.badweather.model.view

enum class Phenomenon {
    CLEAR, FEW_CLOUDS, VARIABLE_CLOUDS, CLOUDY_WITH_CLEAR_SPELLS, CLOUDY, LIGHT_SNOW_SHOWER,
    MODERATE_SNOW_SHOWER, HEAVY_SNOW_SHOWER, LIGHT_SHOWER, MODERATE_SHOWER, HEAVY_SHOWER,
    LIGHT_RAIN, MODERATE_RAIN, HEAVY_RAIN, RISK_OF_GLAZE, LIGHT_SLEET, MODERATE_SLEET,
    LIGHT_SNOWFALL, MODERATE_SNOWFALL, HEAVY_SNOWFALL, SNOWSTORM, DRIFTING_SNOW, HAIL, MIST, FOG,
    THUNDER, THUNDERSTORM
}

fun mapPhenomenon(phenomenon: String): Phenomenon {
    return when (phenomenon) {
        "Clear" -> Phenomenon.CLEAR
        "Few clouds" -> Phenomenon.FEW_CLOUDS
        "Variable clouds" -> Phenomenon.VARIABLE_CLOUDS
        "Cloudy with clear spells" -> Phenomenon.CLOUDY_WITH_CLEAR_SPELLS
        "Cloudy" -> Phenomenon.CLOUDY
        "Light snow shower" -> Phenomenon.LIGHT_SNOW_SHOWER
        "Moderate snow shower" -> Phenomenon.MODERATE_SNOW_SHOWER
        "Heavy snow shower" -> Phenomenon.HEAVY_SNOW_SHOWER
        "Light shower" -> Phenomenon.LIGHT_SHOWER
        "Moderate shower" -> Phenomenon.MODERATE_SHOWER
        "Heavy shower" -> Phenomenon.HEAVY_SHOWER
        "Light rain" -> Phenomenon.LIGHT_RAIN
        "Moderate rain" -> Phenomenon.MODERATE_RAIN
        "Heavy rain" -> Phenomenon.HEAVY_RAIN
        "Risk of glaze" -> Phenomenon.RISK_OF_GLAZE
        "Light sleet" -> Phenomenon.LIGHT_SLEET
        "Moderate sleet" -> Phenomenon.MODERATE_SLEET
        "Light snowfall" -> Phenomenon.LIGHT_SNOWFALL
        "Moderate snowfall" -> Phenomenon.MODERATE_SNOWFALL
        "Heavy snowfall" -> Phenomenon.HEAVY_SNOWFALL
        "Snowstorm" -> Phenomenon.SNOWSTORM
        "Drifting snow" -> Phenomenon.DRIFTING_SNOW
        "Hail" -> Phenomenon.HAIL
        "Mist" -> Phenomenon.MIST
        "Fog" -> Phenomenon.FOG
        "Thunder" -> Phenomenon.THUNDER
        "Thunderstorm" -> Phenomenon.THUNDERSTORM
        else -> Phenomenon.CLEAR
    }
}

fun PhenomenonAnimation(isDay: Boolean, phenomenon: Phenomenon): String {
    return if (isDay) {
        when (phenomenon) {
            Phenomenon.CLEAR -> "sunny.json"
            Phenomenon.FEW_CLOUDS -> "partly_cloudy.json"
            Phenomenon.VARIABLE_CLOUDS -> "partly_cloudy.json"
            Phenomenon.CLOUDY_WITH_CLEAR_SPELLS -> "partly_cloudy.json"
            Phenomenon.CLOUDY -> "cloudy.json"
            Phenomenon.LIGHT_SNOW_SHOWER -> "snow-sunny.json"
            Phenomenon.MODERATE_SNOW_SHOWER -> "snow-sunny.json"
            Phenomenon.HEAVY_SNOW_SHOWER -> "snow-sunny.json"
            Phenomenon.LIGHT_SHOWER -> "partly-shower.json"
            Phenomenon.MODERATE_SHOWER -> "partly-shower.json"
            Phenomenon.HEAVY_SHOWER -> "partly-shower.json"
            Phenomenon.LIGHT_RAIN -> "partly-shower.json"
            Phenomenon.MODERATE_RAIN -> "partly-shower.json"
            Phenomenon.HEAVY_RAIN -> "partly-shower.json"
            Phenomenon.RISK_OF_GLAZE -> "snow-sunny.json"
            Phenomenon.LIGHT_SLEET -> "snow-sunny.json"
            Phenomenon.MODERATE_SLEET -> "snow-sunny.json"
            Phenomenon.LIGHT_SNOWFALL -> "snow-sunny.json"
            Phenomenon.MODERATE_SNOWFALL -> "snow-sunny.json"
            Phenomenon.HEAVY_SNOWFALL -> "snow-sunny.json"
            Phenomenon.SNOWSTORM -> "snow.json"
            Phenomenon.DRIFTING_SNOW -> "snow-sunny.json"
            Phenomenon.HAIL -> "snow.json"
            Phenomenon.MIST -> "mist.json"
            Phenomenon.FOG -> "foggy.json"
            Phenomenon.THUNDER -> "stormshowersday.json"
            Phenomenon.THUNDERSTORM -> "thunder_storm.json"
        }
    } else {
        when (phenomenon) {
            Phenomenon.CLEAR -> "night_clear.json"
            Phenomenon.FEW_CLOUDS -> "cloudynight.json"
            Phenomenon.VARIABLE_CLOUDS -> "cloudynight.json"
            Phenomenon.CLOUDY_WITH_CLEAR_SPELLS -> "cloudynight.json"
            Phenomenon.CLOUDY -> "cloudynight.json"
            Phenomenon.LIGHT_SNOW_SHOWER -> "snownight.json"
            Phenomenon.MODERATE_SNOW_SHOWER -> "snownight.json"
            Phenomenon.HEAVY_SNOW_SHOWER -> "snownight.json"
            Phenomenon.LIGHT_SHOWER -> "rainynight.json"
            Phenomenon.MODERATE_SHOWER -> "rainynight.json"
            Phenomenon.HEAVY_SHOWER -> "rainynight.json"
            Phenomenon.LIGHT_RAIN -> "rainynight.json"
            Phenomenon.MODERATE_RAIN -> "rainynight.json"
            Phenomenon.HEAVY_RAIN -> "rainynight.json"
            Phenomenon.RISK_OF_GLAZE -> "snownight.json"
            Phenomenon.LIGHT_SLEET -> "snownight.json"
            Phenomenon.MODERATE_SLEET -> "snownight.json"
            Phenomenon.LIGHT_SNOWFALL -> "snownight.json"
            Phenomenon.MODERATE_SNOWFALL -> "snownight.json"
            Phenomenon.HEAVY_SNOWFALL -> "snownight.json"
            Phenomenon.SNOWSTORM -> "snow.json"
            Phenomenon.DRIFTING_SNOW -> "snownight.json"
            Phenomenon.HAIL -> "snownight.json"
            Phenomenon.MIST -> "mist.json"
            Phenomenon.FOG -> "foggy.json"
            Phenomenon.THUNDER -> "thunder.json"
            Phenomenon.THUNDERSTORM -> "thunder_storm.json"
        }
    }
}
