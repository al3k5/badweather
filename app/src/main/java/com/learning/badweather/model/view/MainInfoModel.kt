package com.learning.badweather.model.view

import com.learning.badweather.model.utils.formatDate
import org.threeten.bp.LocalDate

data class MainInfoModel(
    val date: LocalDate,
    val tempMax: Int,
    val tempMin: Int,
    val phenomenonText: String,
    val phenomenon: Phenomenon,
    val text: String,
    val sea: String,
    val peipsi: String,
    val winds: List<WindData>,
    val nextDays: List<WeatherData>,
    val placesData: List<WeatherData>
) {
    constructor(isDay: Boolean, forecasts: List<Forecast>) : this(
        forecasts.first().date,
        if (isDay) {
            forecasts.first().day.tempMax
        } else {
            forecasts.first().night.tempMax
        },
        if (isDay) {
            forecasts.first().day.tempmin
        } else {
            forecasts.first().night.tempmin
        },
        if (isDay) {
            forecasts.first().day.phenomenonText
        } else {
            forecasts.first().night.phenomenonText
        },
        if (isDay) {
            forecasts.first().day.phenomenon
        } else {
            forecasts.first().night.phenomenon
        },
        if (isDay) {
            forecasts.first().day.text
        } else {
            forecasts.first().night.text
        },
        if (isDay) {
            forecasts.first().day.sea
        } else {
            forecasts.first().night.sea
        },
        if (isDay) {
            forecasts.first().day.peipsi
        } else {
            forecasts.first().night.peipsi
        },
        if (isDay) {
            forecasts.first().day
        } else {
            forecasts.first().night
        }.winds.map { WindData(it.windDirection, it.speexMax, it.name) },
        forecasts.subList(1, forecasts.size).map { forecast ->
            val weather = if (isDay) forecast.day else forecast.night
            WeatherData(
                isDay,
                weather.phenomenon,
                weather.tempMax,
                formatDate(forecast.date).toString()
            )
        },
        if (isDay) {
            forecasts.first().day
        } else {
            forecasts.first().night
        }.places.map {
            WeatherData(
                isDay, it.phenomenon, it.tempMax ?: it.tempmin, it.name
            )
        }
    )
}

data class WindData(
    val windDirection: WindDirection,
    val windSpeed: Int,
    val title: String
)

data class WeatherData(
    val isDay: Boolean,
    val phenomenon: Phenomenon,
    val temperature: Int?,
    val title: String
)
