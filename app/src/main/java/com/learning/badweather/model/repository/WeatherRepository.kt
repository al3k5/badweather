package com.learning.badweather.model.repository

import com.learning.badweather.model.api.ForecastsService
import com.learning.badweather.model.db.ForecastsDao
import com.learning.badweather.model.dto.toForecast
import com.learning.badweather.model.view.Forecast
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class WeatherRepository @Inject constructor(
    private val forecastsService: ForecastsService,
    private val forecastsDao: ForecastsDao
) {

    fun refresh(): Completable {
        return forecastsService.get()
            .flatMapCompletable { response ->
                Completable.fromAction {
                    forecastsDao.replaceForecasts(response.forecasts)
                }
            }
    }

    fun get(): Observable<List<Forecast>> {
        return forecastsDao.get()
            .map { it.map { forecastDto -> forecastDto.toForecast() } }
    }
}
