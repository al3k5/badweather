package com.learning.badweather.model.api

import com.learning.badweather.model.dto.ForecastDto
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Response(
    val forecasts: List<ForecastDto>
)
