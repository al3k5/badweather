package com.learning.badweather.model.view

data class Weather(
    val phenomenonText: String,
    val phenomenon: Phenomenon,
    val tempmin: Int,
    val tempMax: Int,
    val text: String,
    val sea: String,
    val peipsi: String,
    val places: List<Place>,
    val winds: List<Wind>
)
