package com.learning.badweather.model.view

import org.threeten.bp.LocalDate

data class Forecast(
    val date: LocalDate,
    val day: Weather,
    val night: Weather
)
