package com.learning.badweather.model.api

import io.reactivex.Single
import retrofit2.http.GET

interface ForecastsService {

    @GET("forecast")
    fun get(): Single<Response>
}
