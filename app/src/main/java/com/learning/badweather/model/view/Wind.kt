package com.learning.badweather.model.view

data class Wind(
    val name: String,
    val windDirection: WindDirection,
    val speedMin: Int,
    val speexMax: Int
)
