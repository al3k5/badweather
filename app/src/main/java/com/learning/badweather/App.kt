package com.learning.badweather

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.facebook.stetho.Stetho
import com.learning.badweather.di.ApplicationComponent
import com.learning.badweather.di.DaggerApplicationComponent
import kotlin.reflect.KClass
import android.app.Application as AndroidApplication
import androidx.lifecycle.ViewModel as AndroidViewModel

class App : AndroidApplication() {

    val component by lazy {
        DaggerApplicationComponent.builder()
            .context(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}

val Context.applicationComponent: ApplicationComponent
    get() = (this.applicationContext as App).component

fun <T, M, E> Fragment.getViewModel(type: KClass<T>): ViewModel<M, E>
        where T : AndroidViewModel,
              T : ViewModel<M, E> {
    val factory = (this.context!!.applicationContext as App).component.viewModelFactory()
    return ViewModelProvider(this, factory)[type.java]
}

fun <T, M, E> FragmentActivity.getViewModel(type: KClass<T>): ViewModel<M, E>
        where T : AndroidViewModel,
              T : ViewModel<M, E> {
    val factory = (this.applicationContext as App).component.viewModelFactory()
    return ViewModelProvider(this, factory)[type.java]
}
